#!/bin/python
#
# Script to transcode multi video & audio formats into prores & pcm for import to Davinci Resolve
#
import os
import sys
import subprocess

indir = "/home/antoine/Videos/pre_transcode_in" #Input directory for pre-transcode video & audio files
outdir= "/home/antoine/Videos/transcoded_prores_pcm/" #Output directory for transcoded prores/pcm video & audio files
#
# [ ! -d "$outdir" ] && mkdir "$outdir" #Check if output directory exists, if not, create it.
#

video_ext = ["mkv", "mkv", "m4v", "mp4", "avi", "wmv", "flv", "mov"]
audio_ext = ["flac", "mp3", "aac", "wma", "ogg", "oga", "mogg", "raw"]

for element in os.listdir(indir):
    video = False
    audio = False
    for ext in video_ext:
        if(ext in element or ext.upper() in element):
            video = True
    for ext in audio_ext:
        if(ext in element or ext.upper() in element):
            audio = True
            
    if (video):
        # ffmpeg -i "$file" -c:v prores -profile:v 3 -c:a pcm_s24le "$outdir""${file%.*}".mov;
        file_path_in = os.path.join(indir, element)
        file_path_out = os.path.join(outdir, element.split(".")[0] + ".mov")
        print("****** Starting transcode of %s to %s ******" % (file_path_in, file_path_out))
                                     
        print(subprocess.check_output(['ffmpeg', '-i', file_path_in, '-c:v', 'prores', '-profile:v', '3', '-c:a', 'pcm_s24le', file_path_out]))
        
    elif (audio):
        print("****** Starting transcode of %s to %s ******" % (file_path_in, file_path_out))
        # ffmpeg -i "$file" -c:a pcm_s24le "$outdir""${file%.*}".wav;
        file_path_in = os.path.join(indir, element)
        file_path_out = os.path.join(outdir, element.element.split(".")[0] + ".wav")
                                     
        print(subprocess.check_output(['ffmpeg', '-i', file_path_in, '-c:a', 'pcm_s24le', file_path_out]))
    
    else:
        print("******Error %s is neither audio nor video ******" % element)

